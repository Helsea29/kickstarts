#version=DEVEL
# System authorization information
auth --enableshadow --passalgo=sha512
# Use network installation
url --url="http://mirror.internode.on.net/pub/fedora/linux/releases/24/Server/x86_64/os/" --proxy="http://10.0.20.1:8080"
# Use graphical install
graphical
# Run the Setup Agent on first boot
firstboot --enable
ignoredisk --only-use=vda
# Keyboard layouts
keyboard --vckeymap=us --xlayouts='us'
# System language
lang en_AU.UTF-8

# Network information
network  --bootproto=static --device=ens3 --gateway=10.0.20.1 --ip=10.0.20.212 --nameserver=10.0.20.1 --netmask=255.255.255.0 --noipv6 --activate
network  --hostname=kickstart.deviateit.net
# Root password
rootpw --iscrypted $6$gViEbO//S6yrJ4Eg$XEzJrRuI3NS6abOdiiGQ4VcFaUPSVTvAfyU4G1b6Ng6/nafd4i4MgFcZtoYAsQ5K8/0.e8DdaQeOL0T17ebl51
# System services
services --enabled="chronyd"
# System timezone
timezone Australia/Sydney --isUtc
# System bootloader configuration
bootloader --location=mbr --boot-drive=vda
# Partition clearing information
clearpart --none --initlabel
# Disk partitioning information
part /boot --fstype="ext4" --ondisk=vda --size=256
part pv.164 --fstype="lvmpv" --ondisk=vda --size=43024
volgroup rootvg --pesize=4096 pv.164
logvol /  --fstype="ext4" --size=30720 --name=root --vgname=rootvg
logvol swap  --fstype="swap" --size=2048 --name=swap --vgname=rootvg
logvol /var/log  --fstype="ext4" --size=10240 --name=var_log --vgname=rootvg

%packages
@^server-product-environment
@domain-client
@guest-agents
@headless-management
@web-server
chrony

%end

%addon com_redhat_kdump --disable --reserve-mb='128'

%end

%anaconda
pwpolicy root --minlen=0 --minquality=1 --notstrict --nochanges --emptyok
pwpolicy user --minlen=0 --minquality=1 --notstrict --nochanges --emptyok
pwpolicy luks --minlen=0 --minquality=1 --notstrict --nochanges --emptyok
%end

