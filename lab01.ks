################################################################################
# Kickstart configuration file.
#
# == How To Use
# To use this file you need to run the following sed command
# which will create the correct kickstrt template from this file.
#
# == Boot Linux
# Start linux with the following paramaters::
#
#   inst.ks=https://bitbucket.org/Helsea29/kickstarts/raw/015d0d32f7e1bb5c3144aebe53511723de65610f/lab01.ks
#   ip=10.0.20.222
#   netmask=255.255.255.0
#   gw=10.0.20.1
#   dns=10.0.20.1
#
# Note: Networking configuration is not required
# if you have DHCP.
#


################################################################################
# You will need to set the following.
#
# - Root password
# - Password for bootloader
# - Installation repo
#

rootpw test01
bootloader --location=mbr --append="elevator=noop" --password=7e25a17a322dcc80d77a23d40ebd20ec

################################################################################
# Environment Settings, timezome, keyboard.
#
# - Sydney/Australia Timezome
# - en_US Keyboard (en_au?)
#

lang en_US.UTF-8
timezone Australia/Sydney
keyboard us

################################################################################
# Point to the network install media which will be used
# in your installation. Set the installer to text mode.
#
# - Use a proxy
# - Set elevator to noop for IO performance

text
skipx
cdrom 

################################################################################
# Configure paritioning.
#
# - Using /dev/sda as the install disk
# - Using XFS as the default filesystem
# - Installed into Volume group 'rootvg'
# - Add your own disk configuration here

zerombr
clearpart --all --initlabel
clearpart --all --drives=sda

part /boot --fstype=xfs --size=512
part pv.01 --grow --size=1

volgroup rootvg --pesize=4096 pv.01
logvol /    --fstype=xfs --name=root --vgname=rootvg --size=10240
logvol /var --fstype=xfs --name=var  --vgname=rootvg --size=20480
logvol swap              --name=swap --vgname=rootvg --size=8192

################################################################################
# Configure IP address and networking/firewall configuration
#
# - ens3: 221.121.139.231, Public IP address
# - ens4: DHCP, Management IP address
#

network --bootproto=dhcp

################################################################################
# Default firewall configuration.
#
# Allow:
# - HTTP, FTP, HTTPS, SSH
#

firewall --enabled --http --ftp --ssh

################################################################################
# Other Settings
#
# - Reboot when complete
# - Install instead of upgrade
# - SELinux disabled, enabled in post-config
#

reboot
install
selinux --enforcing

################################################################################
# Software and installation settings.
# Require a number of packages and package groups.
#
# - Packages for basic server
# - Require tGgit for post-init configuration
#
#

repo --name="EPEL" --baseurl=http://dl.fedoraproject.org/pub/epel/7/x86_64/ # for ansible
repo --name="CentOS7" --baseurl=http://mirror.centos.org/centos/7/os/x86_64/
%packages

@Guest Agents

# Install packages
git
rsync
python-dnf
ansible
httpd
mariadb-server
mariadb
php
php-mysql
php-fpm

%end

%post --log=/root/ks-post.log

################################################################################
# Post init configuration.
#
# This section will be run after the installation.
# Use ansible to configure the system.
#
# - Export default configuration files from git
# - Apply default permissions for files which may have been overwritten
#
# - This will download and enable the ks-firstboot service which will continue
#   the build after the system has been rebooted.
#

mkdir -p /var/tmp/bootstrap

# Get SOE Configuration.
git clone "https://Helsea29@bitbucket.org/Helsea29/fedora-soe.git" /var/tmp/bootstrap/config

# Ensure correct permissions.
chmod -R 755 /var/tmp/bootstrap

# Copy Config to root filesystem (TODO: Does this work?)
rsync -rvlH --exclude-from '/var/tmp/bootstrap/config/exclude.txt' /var/tmp/bootstrap/config/* /

# Remove the bootstrap directory
# we are finished with it.
rm -rf /var/tmp/bootstrap

# Restore context to allow
# systemd to exec firstboot.
chmod +x /usr/libexec/firstboot
restorecon /usr/libexec/firstboot

# Enable our first_boot script.
systemctl daemon-reload
systemctl enable ks-firstboot.service

touch /.template

%end
